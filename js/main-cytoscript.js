var last_id = 0;
graph_style = [
{
    selector: 'node',
    css: {
        shape: 'round-rectangle',
        content: 'data(name)',
        width: 160,
        height: 80,
        'border-width': 8,
        'text-wrap': 'wrap',
        'text-max-width': 150,
        'text-halign': 'center',
        'text-valign': 'center',
        "selection-box-color": "#ddd",
        "selection-box-opacity": 0.65,
        "selection-box-border-color": "#aaa",
        "selection-box-border-width": 1,
    }
    },
    {
    selector: 'node[type="bot"]',
    css: {
        'border-color': '#FFAA33',
        'background-color': '#FFCC00',
    }
    },
    {
    selector: 'node[type="human"]',
    css: {
        'border-color': '#0C6E9E',
        'background-color': '#33BBFF',
    }
    },
    {
    selector: 'node[type="emotion"]',
    css: {
        'border-color': '#373737',
        'background-color': 'gray',
    }
    },

    {
    selector: ':selected',
    css: {
        'border-color': '#FF0000',
    }
    },
    {
    selector: 'edge',
    style: {
        'curve-style': 'bezier',
        'target-arrow-shape': 'triangle',
        label: 'data(label)'
    }
    },
    {
    selector: '.eh-handle',
    style: {
        'background-color': 'black',
        width: 22,
        height: 22,
        shape: 'ellipse',
        'overlay-opacity': 0,
        'border-width': 22,
        'border-opacity': 0
    }
    },
    {
    selector: '.eh-hover',
    style: {
        'background-color': 'red'
    }
    },
    {
    selector: '.eh-source',
    style: {
        'border-width': 2,
        'border-color': 'red'
    }
    },
    {
    selector: '.eh-target',
    style: {
        'border-width': 2,
        'border-color': 'red'
    }
    },
    {
    selector: '.eh-preview, .eh-ghost-edge',
    style: {
        'background-color': 'red',
        'line-color': 'red',
        'target-arrow-color': 'red',
        'source-arrow-color': 'red'
    }
    },
    {
    selector: '.eh-ghost-edge.eh-preview-active',
    style: {
        opacity: 0
    }
    }
];

document.addEventListener('DOMContentLoaded', function () {
    var cy_load = localStorage.getItem('graph');
    cy = window.cy = cytoscape({
        container: document.getElementById('cy'),
        layout: {
            name: 'grid',
            rows: 2,
            cols: 2
        },
        style: graph_style,

        elements: {
            nodes: [],
            edges: []
        },
        ready: function () {
            var text = document.getElementById("input_message");
            text.length = 1;
            if (last_node_id == null) {
                for (var i = 0; i < this.nodes().length; i++) {
                    if (this.nodes()[i].data('type') == 'human') {
                        var a = document.createElement("option");
                        a.innerHTML = this.nodes()[i].data('name');
                        text.add(a);
                    }
                }
            } else {
                var phrases_node_ids = null;
                for (var i = 0; i < this.edges().length; i++) {
                    if (this.edges()[i].data('source') == last_node_id) {
                        var target_id = this.edges()[i].data('target');
                        var target_node = this.nodes("[id = '" + target_id + "']");
                        if (target_node.data('type') == 'human') {
                            var a = document.createElement("option");
                            a.innerHTML = target_node.data('name');
                            text.add(a);
                        }
                    }
                }
            }
            this.panningEnabled(true);
            this.zoomingEnabled(true);
            this.boxSelectionEnabled(true);
            this.userZoomingEnabled(true);
            this.userPanningEnabled(true);
        }
    });

    if (cy_load != null) {
        cy_load = JSON.parse(cy_load);
        cy_load['panningEnabled'] = true;
        cy_load['zoomingEnabled'] = true;
        cy_load['boxSelectionEnabled'] = true;
        if (Object.keys(cy_load.elements).length != 0) {
            cy.remove(cy.elements(".eh-handle"));
            cy.json(cy_load);
            cy.remove(cy.elements(".eh-handle"));
            cy.remove(cy.elements(".eh-handle"));
            update_phrases();
            cy.zoomingEnabled(true);
            cy.userZoomingEnabled(true);
            cy.userPanningEnabled(true);
            cy.style(graph_style);
        }
        if ((cy.nodes().length) > 0){
            var max_id = -1;
            for (var i = 0; i < cy.nodes().length; i++){
                var cur_id = parseInt(cy.nodes()[i].data("id"));
                if (cur_id > max_id){
                    max_id = cur_id;
                }
            }
            last_id = max_id + 1;
        }else{
            last_id = 0;
        }
    }


    document.addEventListener('keydown', function(event) {
      if (event.code == 'Delete') {
        cy.remove(cy.$(':selected'));
      }
    });

    cy.on('add', 'node', function (evt) {
        localStorage.setItem('graph', JSON.stringify(cy.json()));
        localStorage.setItem('last_id', last_id);
        update_phrases();
    });

    cy.on('remove', 'node', function (evt) {
        localStorage.setItem('graph', JSON.stringify(cy.json()));
        localStorage.setItem('last_id', last_id);
        update_phrases();
    });

    cy.on('remove', 'edge', function (evt) {
        localStorage.setItem('graph', JSON.stringify(cy.json()));
        localStorage.setItem('last_id', last_id);
        update_phrases();
    });

    cy.cxtmenu({
        selector: 'node[type!="emotion"]',
        commands: [
            {
                content: '<span class="far fa-trash-alt fa-2x"></span>',
                select: function (ele) {
                    cy.remove(ele);
                    update_phrases();
                }
            },

            {
                content: '<span class="far fa-edit fa-2x"></span>',
                select: function (ele) {
                    bootbox.prompt({
                        title: "Введите фразу",
                        centerVertical: true,
                        callback: function (result) {
                          if (result!=null){
                            result = result.replace(/\s+/g,' ').trim();
                            ele.data('name', result);
                            update_phrases();
                          }
                        }
                    });

                },
            },
        ]
    });

    cy.cxtmenu({
        selector: 'node[type="emotion"]',
        commands: [
            {
                content: '<span class="far fa-trash-alt fa-2x"></span>',
                select: function (ele) {
                    cy.remove(ele);
                }
            },
        ]
    });

    cy.cxtmenu({
        selector: 'edge',
        commands: [
            {
                content: '<span class="far fa-trash-alt fa-2x"></span>',
                select: function (ele) {
                    cy.remove(ele);
                }
            },
            {
                content: '<span class="far fa-edit fa-2x"></span>',
                select: function (ele) {
                    bootbox.prompt({
                        title: "Укажите вес",
                        centerVertical: true,
                        callback: function (result) {
                          if (result!=null){
                            result = result.trim();
                            ele.data('label', result);
                            localStorage.setItem('graph', JSON.stringify(cy.json()));
                          }
                        },
                        inputType: "number",
                        step: "any",
                        min: "0",
                        max: "1"
                    });
                },
            },
        ]
    });

    var eh = cy.edgehandles({
      complete: function( sourceNode, targetNode, addedEles ){
        // fired when edgehandles is done and elements are added
        validate(addedEles);
        // var edges = sourceNode.connectedEdges("[source = '" + sourceNode.data("id") + "']");
        // var real_edges = [];
        // for (var i = 0; i < edges.length; i++) {
        //     if (!isNaN(parseInt(edges[i].data("target")))){
        //         real_edges.push(edges[i]);
        //     }
        // }
        // var standart_probability = 1 / real_edges.length;
        // for (var i = 0; i < real_edges.length; i++) {
        //     // if real_edges
        //     console.log(real_edges[i]);
        // }
        // console.log(edges);
        localStorage.setItem('graph', JSON.stringify(cy.json()));
        localStorage.setItem('last_id', last_id);
        update_phrases();
      }
    });


    var nav = cy.navigator();

    cy.nodeResize({
        padding: 5, // spacing between node and grapples/rectangle
        undoable: true, // and if cy.undoRedo exists

        grappleSize: 8, // size of square dots
        grappleColor: 'green', // color of grapples
        inactiveGrappleStroke: 'inside 1px blue',
        boundingRectangleLineDash: [4, 8], // line dash of bounding rectangle
        boundingRectangleLineColor: 'red',
        boundingRectangleLineWidth: 1.5,
        zIndex: 999,

        minWidth: function(node) {
            const data = node.data('resizeMinWidth');
            return data ? data : 15;
        },
        minHeight: function(node) {
            const data = node.data('resizeMinHeight');
            return data ? data : 15;
        },
        resizeToContentCueImage: 'img/resizeCue.svg'
    });
});