var current_emotion = "default";
var server_ip = "https://api.vi-creator.ru/";
var authCode;
var cTitle;
var img_src;

document.addEventListener("DOMContentLoaded", function() {
    var titleElem = document.getElementById('command-title');
    cTitle = localStorage.getItem('command');
    cTitle = (cTitle) ? cTitle : 'User';
    titleElem.textContent = cTitle;
    authCode = localStorage.getItem('authCode');
    authCode = (authCode) ? authCode : 'user';
    img_src = localStorage.getItem('image');
});

function send_JSON(){
        var xhr = new XMLHttpRequest();
        var url = server_ip + "check/" + authCode;
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var answer=JSON.parse(xhr.responseText);
                if (answer != null){
                    console.log(answer);
                    var text = "";
                    for (var i in answer){
                        var line = "<p>" + i + ": " + answer[i] + "</p>";
                        text += line;
                    }
                    bootbox.alert({
                        message: text,
                        centerVertical: true
                    });
                }
            }
        };
        var data = JSON.stringify(cy.json());
        xhr.send(data);
}


function build_emotions_list() {
    for (var i in emotions){
        if (i != "default"){
            add(i);
        }
    }
    localStorage.setItem('emotions', JSON.stringify(emotions));
}

var last_node_id = null;
var emotions = null;
var load_emotions = localStorage.getItem('emotions');

if (load_emotions && load_emotions != "undefined"){
    emotions = JSON.parse(load_emotions);
    if (localStorage.getItem('current_emotion') != null){
        current_emotion = localStorage.getItem('current_emotion');
        update_emotion(current_emotion);
    }
    build_emotions_list();
}else{
    emotions = {
        "default": null
    };
}

function clear_emotion_list(){
    myNode = document.getElementById("emotions_list");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
}

function update_emotion() {
    document.getElementById("emotion_text").innerHTML = current_emotion;
    document.getElementById("bot_gif").src = emotions[current_emotion];
    localStorage.setItem('current_emotion', current_emotion);
}


function validate(edge_for_delete) {
    var u = cy.$("#" + edge_for_delete.data("source"));
    var v = cy.$("#" + edge_for_delete.data("target"));
    var count_edges = cy.edges('[source="' + u.data("id") + '"]').edges('[target="' + v.data("id") + '"]').length;
    if  (count_edges > 1){
        cy.remove(edge_for_delete);
        bootbox.alert({
            message: "Нельзя ставить более одного ребра в одном направлении между двумя узлами",
            centerVertical: true
        })
    }else if (v.data("type") == "human" && u.data("type") == "human") {
        cy.remove(edge_for_delete);
        bootbox.alert({
            message: "Нельзя ставить две фразы человека подряд",
            centerVertical: true
        })
    } else if (v.data("type") == "human" && u.data("type") == "emotion") {
        cy.remove(edge_for_delete);
        bootbox.alert({
            message: "Нельзя ставить эмоцию перед фразой человека",
            centerVertical: true
        })
    } else if (v.data("type") == "emotion" && u.data("type") == "emotion") {
        cy.remove(edge_for_delete);
        bootbox.alert({
            message: "Нельзя ставить две эмоции",
            centerVertical: true
        })
    } else if (v.data("type") == "emotion" && u.data("type") == "bot") {
        cy.remove(edge_for_delete);
        bootbox.alert({
            message: "Нельзя ставить эмоцию после фразы бота",
            centerVertical: true
        })
    }
}

function add_bot_message(message) {
    var element = document.createElement("p");
    element.innerHTML = message;
    element.classList.add('bot_message');
    var foo = document.getElementById("chat");
    foo.appendChild(element);
    foo.scrollTop = foo.scrollHeight;
}

function bfs_s(current_node_id) {
    var levels = {};
    var passed_on_levels = {};
    var last_edge;
    var bfs = cy.elements().bfs({
        roots: '#' + current_node_id,
        visit: function(v, e, u, i, depth) {
            if (u && u.connectedEdges("[source = '" + u.data("id") + "']").length == 1) {
                last_edge = e;
                return true;
            }
            if (u && u.connectedEdges("[source = '" + u.data("id") + "']").length == 1) {
                return true;
            } else if (e && (depth in levels) && (u.connectedEdges("[source = '" + u.data("id") + "']").length - 1 == passed_on_levels[depth])) {
                last_edge = e;
                return true;
            } else if (e && (!(depth in levels) || levels[depth] == false)) {

                if (!(depth in levels)) {
                    passed_on_levels[depth] = 1;
                } else {
                    passed_on_levels[depth]++;
                }

                if (e.data('label') != 0 && (1 - e.data('label')) < Math.random()) {
                    levels[depth] = true;
                    last_edge = e;
                    return true;
                } else {
                    levels[depth] = false;
                }
            } else {

            }
        },
        directed: true
    });
    path = bfs.path; // path to found node
    found = bfs.found; // found node
    if (found.data("type") == "human") {
        last_node_id = current_node_id;
        return;
    }
    if (found.data() == null) {
        if (path[path.length-1].data("type")!="bot"){
            add_bot_message('Мне нечего ответить(((');
        }
        last_node_id = current_node_id;
        update_phrases();
        return;
    }
    cy.edges().unselect();
    last_edge.select();
    if (found[0].data("type") == "emotion") {
        current_emotion = found[0].data("name");
        update_emotion(current_emotion);
        add_bot_message('*** Смена эмоции на "' + current_emotion + '" ***');
        last_node_id = found[0].data("id");
        return bfs_s(found[0].data("id"));
    }
    if (found[0].data("type") == "bot") {
        if (cy.nodes("[id = '" + current_node_id + "']").data("type") != "emotion") {
            current_emotion = "default";
            update_emotion(current_emotion);
        }
        last_node_id = found[0].data("id");
        var message = found[0].data('name');
        add_bot_message(message);
        // if (found[0].connectedEdges("[source = '" + found[0].data("id") + "']").length == 1) {
        return bfs_s(found[0].data("id"));
        // }
    }
    return found[0];
}

function send_bot_answer(current_node_id) {
    var result_node = bfs_s(current_node_id);
    update_phrases();
}

function update_phrases() {
    var text = document.getElementById("input_message");
    text.length = 1;
    if (last_node_id == null) {
        for (var i = 0; i < cy.nodes().length; i++) {
            if (cy.nodes()[i].data('type') == 'human') {
                var a = document.createElement("option");
                a.innerHTML = cy.nodes()[i].data('name');
                a.id = cy.nodes()[i].data('id');
                text.add(a);
            }
        }
    } else {
        for (var i = 0; i < cy.edges().length; i++) {
            if (cy.edges()[i].data('source') == last_node_id) {
                var target_id = cy.edges()[i].data('target');
                var target_node = cy.nodes("[id = '" + target_id + "']");
                if (target_node.data('type') == 'human') {
                    var a = document.createElement("option");
                    a.innerHTML = target_node.data('name');
                    a.id = target_node.data('id');
                    text.add(a);
                }
            }
        }
    }
}

function send_message() {
    var input = document.getElementById("input_message");
    if (input.selectedIndex > 0) {
        var option = input.options[input.selectedIndex];
        text = option.text;
        text = text.replace(/\s+/g,' ').trim();
        var element = document.createElement("p");
        element.innerHTML = text;
        element.classList.add('human_message');
        var foo = document.getElementById("chat");
        foo.appendChild(element);
        foo.scrollTop = foo.scrollHeight;
        send_bot_answer(option.id);
    }
}

function get_emotion_child(node){
    if (node){
        var next_node = node.connectedEdges("[source = '" + node.data("id") + "']")[0];
        if (next_node){
            return cy.$('#' + next_node.data("target"));
        }
    }
}



document.getElementById("download-phrases").addEventListener("click", function() {
    var phrases_list = {};
    var human_phrases = cy.nodes("[type = 'human']");
    for (var i = 0; i < human_phrases.length; i++){
        var target = human_phrases[i].connectedEdges("[source = '" + human_phrases[i].data("id") + "']");
        phrases_list[human_phrases[i].data("name")] = [];
        for (var j = 0; j < target.length; j++){
            var node = cy.$('#' + target[j].data("target"));
            if (node.data("type")=="emotion"){
                node = get_emotion_child(node);
            }else{
                node = cy.$('#' + target[j].data("target"));
            }
            if (node){
                phrases_list[human_phrases[i].data("name")].push(node.data("name"));
            }
        }        
    }
    var text = "";
    for (var i in phrases_list){
        text += i;
        for (var j in phrases_list[i]){
            line = "\n - " + phrases_list[i][j];
            text += line;
        }
        text += "\n\n";
    }

    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(text);
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href", dataStr);
    downloadAnchorNode.setAttribute("download", "dialog" + ".txt");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
});


document.getElementById('python-fileid').addEventListener('change', load_python_file, false);
function load_python_file(evt) {
    var file = evt.target.files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
        var python_file = e.target.result;
        var xhr = new XMLHttpRequest();
        var url = server_ip + "test/" + authCode;
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var answer=JSON.parse(xhr.responseText);
                console.log(answer);
                bootbox.alert({
                    message: answer['message'],
                    centerVertical: true
                });
            }
        };
        var data = python_file;
        xhr.send(data);
    };
    reader.readAsText(file);
    document.getElementById("python-fileid").value = "";
};
document.getElementById("upload-python").addEventListener("click", function() {
    document.getElementById('python-fileid').click();    
});


document.getElementById("upload-to-vk").addEventListener("click", function() {
    var xhr = new XMLHttpRequest();
    var url = server_ip + "vkbot/" + authCode;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var answer=JSON.parse(xhr.responseText);
            console.log(answer);
            bootbox.alert({
                message: answer['message'],
                centerVertical: true
            });
        }
    };
    xhr.send("");
});

document.getElementById("help").addEventListener("click", function() {
    var info = JSON.parse(localStorage.getItem("info"));
    var text = "";
    for (var i in info){
        var line = "<p>" + i + ": " + info[i] + "</p>";
        text += line;
    }
    if (text){
    	if (img_src) {
    		text += "<img src=" + img_src + " style='width: 100%;'></img>";	
    	}
        bootbox.alert({
            message: text,
            centerVertical: true
        });
    }else{
        bootbox.alert({
            message: "Вы не авторизованы",
            centerVertical: true
        });
    }
});

document.getElementById("check").addEventListener("click", function() {
    send_JSON();
});

document.getElementById("clear_all").addEventListener("click", function() {
    localStorage.removeItem("emotions");
    localStorage.removeItem("current_emotion");
    localStorage.removeItem("last_id");
    localStorage.removeItem("graph");
    location.reload(true);
});


document.getElementById("refresh").addEventListener("click", function() {
    last_node_id = null;
    current_emotion = "default";
    update_emotion(current_emotion);
    myNode = document.getElementById("chat");
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    update_phrases();
});

document.getElementById("download").addEventListener("click", function() {
    var data = cy.json();
    data["emotions"] = emotions;
    var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(data));
    var downloadAnchorNode = document.createElement('a');
    downloadAnchorNode.setAttribute("href", dataStr);
    downloadAnchorNode.setAttribute("download", "graph" + ".json");
    document.body.appendChild(downloadAnchorNode); // required for firefox
    downloadAnchorNode.click();
    downloadAnchorNode.remove();
});

document.getElementById('fileid').addEventListener('change', upload_json, false);
function upload_json(evt) {
    var file = evt.target.files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
        var data = JSON.parse(e.target.result);
        cy.remove(cy.elements(".eh-handle"));
        data['panningEnabled'] = true;
        data['zoomingEnabled'] = true;
        data['boxSelectionEnabled'] = true;
        cy.json(data);
        cy.userZoomingEnabled(true);
        cy.userPanningEnabled(true);
        cy.remove(cy.elements(".eh-handle"));
        if ((cy.nodes().length) > 0){
            var max_id = -1;
            for (var i = 0; i < cy.nodes().length; i++){
                var cur_id = parseInt(cy.nodes()[i].data("id"));
                if (cur_id > max_id){
                    max_id = cur_id;
                }
            }
            last_id = max_id + 1;
        }else{
            last_id = 0;
        }
        localStorage.setItem('graph', JSON.stringify(data));
        localStorage.setItem('last_id', last_id);
        emotions = data["emotions"];
        localStorage.setItem('emotions', JSON.stringify(emotions));
        clear_emotion_list();
        build_emotions_list();
    };
    document.getElementById("fileid").value = "";
    reader.readAsText(file);
};


document.getElementById("upload").addEventListener("click", function() {
    localStorage.removeItem("emotions");
    localStorage.removeItem("current_emotion");
    localStorage.removeItem("last_id");
    localStorage.removeItem("graph");
    document.getElementById('fileid').click();
});


document.getElementById("reset").addEventListener("click", function() {
    cy.fit(cy);
});


function add_bot_phrase() {
    cy.add({
        group: 'nodes',
        data: {
            id: last_id,
            name: "фразы бота",
            type: 'bot'
        },
        position: {
            x: 0,
            y: 0
        }
    });
    last_id++;
}

function add_human_phrase() {
    cy.add({
        group: 'nodes',
        data: {
            id: last_id,
            name: "фразы человека",
            type: 'human'
        },
        position: {
            x: 0,
            y: 0
        }
    });
    last_id++;
    update_phrases();
}

function create_emotion(evt) {
    bootbox.prompt({
        title: "Введите название эмоции",
        centerVertical: true,
        callback: function(result) {
            if (result){
                result = result.trim();
                emotions[result] = null;
                add(result);
                localStorage.setItem('emotions', JSON.stringify(emotions));
            }
        }
    });
};
document.getElementById('image').addEventListener('click', create_emotion, false);

function load_default_emotion(evt) {
    var file = evt.target.files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
        emotions["default"] = e.target.result;
        if (current_emotion == "default") {
            update_emotion(current_emotion);
        }
        localStorage.setItem('emotions', JSON.stringify(emotions));
    };
    reader.readAsDataURL(file);
};
document.getElementById('default_emotion').addEventListener('change', load_default_emotion, false);

function add_emotion(){
    cy.add({
        group: 'nodes',
        data: {
            id: last_id,
            name: this.value,
            type: 'emotion'
        },
        position: {
            x: 0,
            y: 0
        }
    });
    last_id++;
}

function remove_emotion(){
    cy.remove('node[name = "' + this.parentNode.id + '"]');
    delete(emotions[this.parentNode.id]);
    if (current_emotion == this.parentNode.id) {
        current_emotion = "default";
        update_emotion(current_emotion);
    }
    localStorage.setItem('emotions', JSON.stringify(emotions));
    this.parentNode.parentNode.removeChild(this.parentNode);
}

function add(value) {
    var foo = document.getElementById("emotions_list");

    var element = document.createElement("div");
    element.id = value;
    element.classList.add('emotion_line');
    foo.appendChild(element);

    var element_b = document.createElement("input");
    element_b.type = "button";
    element_b.value = value;
    element_b.classList.add('emotion');
    element_b.onclick = add_emotion;
    element.appendChild(element_b);

    var element_c = document.createElement("a");
    element_c.classList.add("delete_emotion");
    element_c.innerHTML = '<i class="fas fa-trash-alt fa-2x icon_center"></i>';
    element_c.onclick = remove_emotion;
    element.appendChild(element_c);


    var element_e = document.createElement("label");
    element_e.classList.add("load_emotion");
    element_e.innerHTML = '<i class="fas fa-edit fa-2x icon_center"></i>';
    element_e.htmlFor = value + "_label";
    element.appendChild(element_e);

    var element_d = document.createElement("input");
    element_d.classList.add("inputfile");
    element_d.type = "file";
    element_d.id = value + "_label";
    element_d.accept=".gif, .jpg, .jpeg, .png";
    element_d.addEventListener('change', load_emotion_image, false);
    element.appendChild(element_d);
}

function load_emotion_image(evt){
    var emotion_name = evt["target"].parentNode.id;
    var file = evt.target.files[0];
    var reader = new FileReader();
    reader.onload = function(e) {
        emotions[emotion_name] = e.target.result;
        localStorage.setItem('emotions', JSON.stringify(emotions));
        if (current_emotion == emotion_name){
            update_emotion(emotion_name);
        }
        localStorage.setItem('emotions', JSON.stringify(emotions));
    };
    reader.readAsDataURL(file);
}